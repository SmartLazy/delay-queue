package com.smartlazy.delay.queue.core.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author howe
 */
@Data
@ApiModel
public class JobItem implements Serializable {
    @ApiModelProperty(value = "Job类型。可以理解成具体的业务名称。", example = "订单")
    private String topic;
    @ApiModelProperty(value = "job唯一标识ID", example = "UUID")
    private String id;
    @ApiModelProperty(value = "Job需要延迟的时间。单位：秒。（服务端会将其转换为绝对时间）", example = "600")
    private int delay;
    @ApiModelProperty(value = "time-to-run：Job执行超时时间。单位：秒。", example = "30")
    private int ttr;
    @ApiModelProperty(value = "到期时间 单位：毫秒", example = "1000")
    private long timestamp;
    @ApiModelProperty(value = "Job的内容，供消费者做具体的业务处理，以json格式存储。", example = "30")
    private String body;
}
