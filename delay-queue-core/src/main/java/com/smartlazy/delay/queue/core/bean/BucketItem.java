package com.smartlazy.delay.queue.core.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author howe
 */
@Data
public class BucketItem implements Serializable {
    @ApiModelProperty(value = "job唯一标识ID", example = "UUID")
    private String jobId;
    @ApiModelProperty(value = "到期时间", example = "1000")
    private double timestamp;
}
