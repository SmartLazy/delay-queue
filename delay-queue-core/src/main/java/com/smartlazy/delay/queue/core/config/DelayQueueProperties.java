package com.smartlazy.delay.queue.core.config;

import com.smartlazy.delay.queue.core.constant.CommonConst;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author howe
 */
@Data
@ConfigurationProperties(prefix = DelayQueueProperties.OSS_PREFIX)
public class DelayQueueProperties {
    public static final String OSS_PREFIX = "delay.queue";
    private Integer blockTimeout = CommonConst.DEFAULT_QUEUE_BLOCK_TIMEOUT;
    private Integer bucketSize = CommonConst.DEFAULT_BUCKET_SIZE;
}
