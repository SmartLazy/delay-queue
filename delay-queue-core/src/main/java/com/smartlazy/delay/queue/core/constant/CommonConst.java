package com.smartlazy.delay.queue.core.constant;

/**
 *
 * @author howe
 */
public class CommonConst {
	private CommonConst() {
	}
	/**
	 * BUCKET_KEY
	 */
	public static final String DEFAULT_BUCKET_KEY = "dq_bucket_";
	/**
	 * QUEUE_KEY
	 */
	public static final String DEFAULT_QUEUE_KEY = "dq_queue_";
	/**
	 * JOB_KEY
	 */
	public static final String DEFAULT_JOB_KEY = "dq_job_";
	/**
	 * 轮询队列超时时间
	 */
	public static final int DEFAULT_QUEUE_BLOCK_TIMEOUT = 30;
	/**
	 * bucket数量
	 */
	public static final int DEFAULT_BUCKET_SIZE = 3;
}
