package com.smartlazy.delay.queue.core.config;

import com.smartlazy.delay.queue.core.bean.CircleQueue;
import com.smartlazy.delay.queue.core.service.DelayBucket;
import com.smartlazy.delay.queue.core.service.DelayJob;
import com.smartlazy.delay.queue.core.service.DelayQueue;
import com.smartlazy.delay.queue.core.service.ReadyQueue;
import com.smartlazy.delay.queue.core.timer.Timer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author howe
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(DelayQueueProperties.class)
public class DelayQueueAutoConfig {

    @Autowired
    private DelayQueueProperties delayQueueProperties;

    @Autowired
    private DelayBucket delayBucket;

    @Autowired
    private DelayJob delayJob;

    @Autowired
    private ReadyQueue readyQueue;

    @Autowired
    private DelayQueue delayQueue;

    /**
     * 注入定时器
     */
    @Bean
    public Timer timer(){
        Timer timer = new Timer();
        timer.setDelayBucket(delayBucket);
        timer.setDelayJob(delayJob);
        timer.setReadyQueue(readyQueue);
        timer.setDelayQueueProperties(delayQueueProperties);
        timer.initTimer();
        CircleQueue<String> queue = new CircleQueue<String>(delayQueueProperties.getBucketSize());
        delayQueue.initBucketName(queue);
        return timer;
    }
}
