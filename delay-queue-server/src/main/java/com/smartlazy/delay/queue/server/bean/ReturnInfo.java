package com.smartlazy.delay.queue.server.bean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author howe
 * @param <T>
 */
@Data
@ApiModel
public class ReturnInfo<T> {
    @ApiModelProperty(value = "状态码,1 表示成功 其他表示失败", example = "0")
    private int returnCode = 1;

    @ApiModelProperty(value = "错误代码", example = "200")
    private String errorCode = "200";

    @ApiModelProperty(value = "错误信息", example = "操作成功")
    private String errorText = "成功";

    @ApiModelProperty(value = "数据", example = "")
    private T data;
}