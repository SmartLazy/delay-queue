package com.smartlazy.delay.queue.server.web;

import com.smartlazy.delay.queue.core.bean.JobItem;
import com.smartlazy.delay.queue.core.service.DelayQueue;
import com.smartlazy.delay.queue.server.bean.ReturnInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author howe
 */
@Slf4j
@Controller
@RequestMapping("/")
public class IndexController {

    @Autowired
    private DelayQueue delayQueue;

    /**
     * 添加job
     * @param input
     * @return
     */
    @RequestMapping(value = "/push", method = RequestMethod.POST)
    @ResponseBody
    public ReturnInfo<Long> push(JobItem input) {
        ReturnInfo ret = new ReturnInfo();
        try {
            Boolean data = delayQueue.push(input);
            ret.setData(data);
            ret.setErrorCode("200");
            ret.setErrorText("成功");
            ret.setReturnCode(1);
        } catch (Exception e) {
            ret.setErrorCode("500");
            ret.setErrorText("系统异常");
            ret.setReturnCode(2);
            log.error("系统异常", e);
        }
        return ret;
    }

    /**
     * 获取job
     * @param input
     * @return
     */
    @RequestMapping(value = "/pop", method = RequestMethod.POST)
    @ResponseBody
    public ReturnInfo<JobItem> pop(String input) {
        ReturnInfo ret = new ReturnInfo();
        try {
            JobItem data = delayQueue.pop(input);
            if (data == null) {
                ret.setErrorCode("999");
                ret.setErrorText("没有找到要执行的job");
                ret.setReturnCode(1);
                return ret;
            }
            ret.setData(data);
            ret.setErrorCode("200");
            ret.setErrorText("成功");
            ret.setReturnCode(1);
        } catch (Exception e) {
            ret.setErrorCode("500");
            ret.setErrorText("系统异常");
            ret.setReturnCode(2);
            log.error("系统异常", e);
        }
        return ret;
    }

    /**
     * 删除job
     * @param input
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ReturnInfo<Long> delete(JobItem input) {
        ReturnInfo ret = new ReturnInfo();
        try {
            Boolean data = delayQueue.remove(input.getId());
            ret.setData(data);
            ret.setErrorCode("200");
            ret.setErrorText("成功");
            ret.setReturnCode(1);
        } catch (Exception e) {
            ret.setErrorCode("500");
            ret.setErrorText("系统异常");
            ret.setReturnCode(2);
            log.error("系统异常", e);
        }
        return ret;
    }

    /**
     * 查询job
     * @param input
     * @return
     */
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    @ResponseBody
    public ReturnInfo<JobItem> get(JobItem input) {
        ReturnInfo ret = new ReturnInfo();
        try {
            JobItem data = delayQueue.get(input.getId());
            ret.setData(data);
            ret.setErrorCode("200");
            ret.setErrorText("成功");
            ret.setReturnCode(1);
        } catch (Exception e) {
            ret.setErrorCode("500");
            ret.setErrorText("系统异常");
            ret.setReturnCode(2);
            log.error("系统异常", e);
        }
        return ret;
    }
}
