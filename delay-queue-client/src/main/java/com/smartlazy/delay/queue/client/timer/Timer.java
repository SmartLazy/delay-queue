package com.smartlazy.delay.queue.client.timer;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author howe
 */
public class Timer {

    /**
     * 扫描bucket, 取出延迟时间小于当前时间的Job
     */
    public void initTimer() {
        ScheduledExecutorService service = new ScheduledThreadPoolExecutor(1,
                new BasicThreadFactory.Builder().namingPattern("schedule-pool-%d").daemon(true).build());
        // 第二个参数为首次执行的延时时间，第三个参数为定时执行的间隔时间
        service.scheduleAtFixedRate(new TickHandler("client"),
                10,
                1,
                TimeUnit.SECONDS);
    }
}
