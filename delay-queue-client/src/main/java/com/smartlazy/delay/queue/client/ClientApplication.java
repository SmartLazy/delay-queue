package com.smartlazy.delay.queue.client;

import com.smartlazy.delay.queue.client.timer.Timer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author howe
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.smartlazy.delay.queue.client"})
@Slf4j
public class ClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
        Timer timer = new Timer();
        timer.initTimer();
    }
}
