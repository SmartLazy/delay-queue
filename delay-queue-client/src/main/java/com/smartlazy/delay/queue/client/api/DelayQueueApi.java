package com.smartlazy.delay.queue.client.api;

import com.github.youzan.httpfetch.Header;
import com.github.youzan.httpfetch.HttpApi;
import com.github.youzan.httpfetch.QueryParam;
import com.github.youzan.httpfetch.chains.BeanParam;
import com.smartlazy.delay.queue.client.bean.JobItem;
import com.smartlazy.delay.queue.client.bean.ReturnInfo;

/**
 * @author howe
 */
public interface DelayQueueApi {
    /**
     *
     * 新增一个 job
     */
    @HttpApi(timeout = 2000,
            url = "http://localhost:10010/push",
            method="POST",
            headers = {@Header(key="Content-Type", value = "application/json;charset=utf-8;")})
        ReturnInfo<Boolean> push(@BeanParam JobItem input);
    /**
     *
     * 获取job
     */
    @HttpApi(timeout = 2000,
            url = "http://localhost:10010/pop",
            method="POST",
            headers = {@Header(key="Content-Type", value = "application/json;charset=utf-8;")})
    ReturnInfo<JobItem> pop(@QueryParam("input") String input);
    /**
     *
     * 删除job
     */
    @HttpApi(timeout = 2000,
            url = "http://localhost:10010/delete",
            method="POST",
            headers = {@Header(key="Content-Type", value = "application/json;charset=utf-8;")})
    ReturnInfo<Boolean> delete(@BeanParam JobItem input);
}
